# Breaking ties in approval voting

An election using approval voting may need to break ties.

Before the election begins, the election organizer should declare what method will be used to break ties.

## In-person tie-breaking

If the election is held in person, where the electorate can physically monitor the results, the simplest approach to tie breaking is to use dice, cards, straws, or coins to select one of the tied candidates.

### Using cards to break a tie

Cards are probably the easiest approach.

To break a tie between a group of N candidates, prepare N cards with identical backs.
The face of one of the cards should differ from the others.

The election organizer shuffles the cards face-down in front of everyone, and each candidate draws a card.

The candidate with the card with the distinct face wins the tie.

## Remote tie-breaking

When some part of the electorate can't be physically present, but they want to be able to confirm that ties are broken randomly, the easiest way is to declare a concrete way to discover a high-entropy seed S shortly after the election is over.
This seed is used in combination with a cryptographic hash function X to break ties between candidates.

The seed must have the following properties:

- it must be _unpredictable_ by anyone participating in the election before the election is over (no one should be able to guess it with high probability; as a rule of thumb, it should probably have at least as many bits of entropy as there are candidates in the contest)
- it must be _uncontrollable_ by anyone participating in the election (no one should be able to influence it)
- it must be _publicly-verifiable_ by anyone participating in the election (everyone should be able to agree on it)

Each candidate identifies themself with a unique label L, which is a text string in UTF-8 with no line breaks.

If a tie needs to be broken between a group of candidates, each candidate is assigned a score Z = X(S || L).
The candidate with the lowest Z wins the tie.

There are different ways to choose such a seed.
The election organizer should declare how the seed will be chosen in advance of the election.

### Using a seed from a well-known public ledger

The election organizer may declare that the seed will be derived from an independent, well-known public ledger.

For example, a reasonable choice could be something like:

> The seed will be the hash value of the first Bitcoin block mined after the conclusion of the election, represented as a string of 256 bits.

Another example would be:

> The seed will be the value of the `sha256_root_hash` value from the Let's Encrypt Oak 2022 Certificate Transparency Signed Tree Head (as found at https://oak.ct.letsencrypt.org/2022/ct/v1/get-sth) for the first STH issued after the conclusion of the election, represented as a string of 256 bits.

Note that this may delay the election results, depending on the cadence of the particular ledger selected.

Note also that it's possible that if the ledger is distributed, it can fail to reach consensus for a while, e.g. during an accidental fork.
It's also possible for some ledgers to collapse or to have an expiration date.
The election organizer should consider the community health and expected duration of a particular ledger before selecting to use it for a given election.

### Using a seed from a well-known source

The election organizer may declare that the seed will be derived from a globally-recognized source of unpredictable information, like news or the weather.

A source used in this way should produce standard, relatively frequent updates that are archived and presented uniformly no matter who is accessing them.

For example, the organizer might specify:

> The seed will be the the SHA2-256 digest of meteorological data from NOAA buoy 44005 for the hour after the conclusion of the election.
> The input to the digest function will be the single line of US-ASCII text corresponding to the appropriate time as derived from https://www.ndbc.noaa.gov/data/5day2/44005_5day.txt, with no line endings.

When selecting a well-known source of this type, the election organizer should be aware that some information sources from the real world may fail to generate results due to hardware or communications failures, and these failures could delay the resolution of any ties.

The election organizer should also ensure that the well-known source does in fact have sufficient entropy to be sufficiently unpredictable.

It is also possible that these records may disappear or be overwritten, as not every well-known source keeps a permanent archive.
It is imperative that anyone with a stake in the outcome of any tied election results audit the tie-breaking within the expiration window of the source.

### Generating a seed from data in the election itself

The seed could be generated from data supplied by the election itself, for example, from the balloting process.

To do this in an unobjectionable manner, no proper subset of the electorate ought to be able to conspire to control the tie-breaking mechanism against the wishes of the other participants.

This section describes one way to do this, though there are other approaches.
It depends on the same cryptographic hash function X.

It assumes that each submitted ballot is a text file in UTF-8.
While the ballot can be cleanly parsed to determine intent of the elector, there is also designated room for the elector to insert arbitrary text that will not change the semantics of the ballot.
This allows the digest of each ballot to be decoupled from its semantics.

Each voter V writes their ballot as a UTF-8-encoded string B, and publicly commits to it by publishing commitment C = X(B).
If a voter trusts the rest of the electorate to not collude to fix the tie-breaking seed, the voter may also simultaneously publish B itself to vote in a single transaction.

Once all commitments C are published or some pre-defined time-limit expires, the voting is over.
Every voter that has not already published their ballot B reveals it.
(This may require an additional, subsequent deadline for voters to follow up with this second phase)

Every ballot B that matches the voter's commitment C and can be parsed correctly is a legitimate ballot.

All legitimate ballots are sorted by a pre-defined standard sorting algorithm, and the sorted ballots are concatenated into a single UTF-8-encoded string Z.

X(Z) is then used as the seed for tie-breaking.
