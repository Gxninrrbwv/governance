Present: Daniel, Lukas, Ola, Vincent
Absent: 
Absent with excuses: Neal

# Agenda

* Operations team report
* Raising rate limits in general
* Update from IETF 117 by Daniel
* Discuss the API proposal and handling v6 keys in general


# Operations team report
* debian dist-upgrade is pending for bookworm


# Raising rate limits in general
* Someone from another project has issues with the rate limit
  * They are making distributed requests with different instances
  * They have both, direct requests from users but also on-premise instances which have the same IP address
  * They propose about 10x higher rate limit
  * Vincent: too high rate limit makes it attractive for scraping. Was the reason why the rate limit was introduced in the first place, since scraping had been noticed
  * They don't have approached Vincent directly yet
  * They have their own keyserver but would consider switching to KOO. Not high priority yet though
  * Vincent: we don't necessarily want to cover workflows from companies that could run their own keyserver to not support the off-loading of the responsibility to KOO
  * Vincent: could check how many requests run into rate-limits (HTTP 429) and decide based on that


# Update from IETF 117 by Daniel
* Andrew Gallagher wants to add something to the proposal for HKP
* If that happens the working group will discuss how to handle v6 keys via HKP
* Vincent: Better than brewing our own thing
* Vincent: If good we could drop KOO's API for it
* Daniel: Will probably take a long time


## Status of crypto-refresh
* Has been requested to be published
* To be decided by higher ups
* There are currently no more blockers according to Daniel
* It's possible that Werner puts in a complaint that the process was not properly followed


# Discuss the API proposal and handling v6 keys in general
Daniel: Proton has one person with Rust experience on the crypto team. Might be able to implement the proposal.
Vincent: Proposal should later be more public and people should be given a chance to chime in
Vincent: Client should not necessarily have to make the decision which key should be used in case of multiple versions for one email address being available
Daniel: The client should prepare the key block with multiple version of the key. The sever will simply store that. First perform the validation however.
Lukas: I believe the key owner should not have to decide which key should be delivered
Vincent: In the admin interface where you can manage your key for an email address, the user can add additional keys and KOO will create a key block with all of them ordered by creation date
Vincent: Easiest solution for client would be to have one endpoint for V4 and one for V6 keys
Vincent: We could also have two endpoints for uploading. Would result in 2 verification emails
Lukas: Preferred would be that the two could also be uploaded together, so only 1 verification email is sent
Ola: We don't have enough information what workflows will be, so shouldn't necessarily be worked into the API yet.
Vincent: Later on we could have a V4+V6 endpoint.
Daniel: Baking the version into the API is the wrong place. It would make more sense to have the current API supporting a single key and a different version of the API would support multiple keys (v4,v6)
Ola: Conflating two issues. One is supporting V6 keys and the other is support multiple keys. Support v6 keys now is easy comparted to supporting multiple keys



# Next Meeting on August 25th 2023
