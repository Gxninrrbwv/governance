Present: Neal, Vincent, Ola, Daniel, Lukas
Absent:
Absent with excuses:

# Agenda

* Remain on Gitlab or switch to alternative.
* Decide if a code of conduct should be used when communicating via public channels (mailing list, repository)
* What happened on operations side
* Discuss technical backlog
* Authenticate fingerprints of board members
* Talk about how to proactively invite new members which were not previously fitting the criteria.
* Discuss the process for adding members to the Operations team
* What does the Operations team do
* Discuss open merge requests / formal enhancement proposals

# Remain on Gitlab or switch to alternative.
* Still unclear when Gitlab will implement new restrictions
* We'll wait, no need to act now


# Adopt a Code of Conduct?
* All are in favor of adopting a CoC
* Yet to decide on which one to use


# What happened on operations side
* No support requests
* Holidays
* Registered openpgp.io - could be setup as a fallback for keys.openpgp.org already


# Technical Backlog
* Not compiled yet.
* Vincent will prepare the items and sent them out to the board on the weekend.


# Authenticate fingerprints of board members
* All members added their key pair fingerprints to a file
* Each member read 4 letters of the sha256 in order until it was complete
* SHA256 of the file, verified by all 5 board members:
```
   1d5ce56152933a44950f228e60a97f50ee5126df91e678b4814edd0b03e73729  koo-board-keys.txt
```


# How to proactively invite new members
* Send invite to following mailing lists: ietf openpgp, sks-devel, gnupg-users, koo-voting-request


# Adding members to the Operations Team
* A potential candidate has expressed interest. Vincent will reach out.


# Tasks of Operations team
* Support email
  - questions regarding how the server runs
  - user support requests
* Make sure the server doesn't burn
* Deployment of code
* Maintain Hagrid codebase
* Backups


# Open merge requests / Formal enhancement proposal
* Vincent gave quick overview of the open merge requests

# Next Agenda (Next Meeting: January 20th 2023, 1.30pm UTC)
* Discuss in detail how to handle third-party certifications in KOO


# Action Items
- [ ] Lukas: look into available CoC's and talk to people who might have more experience
- [ ] Vincent: send out voting body invites
- [ ] Neal: send invites to the individual members we would like to add to the voting body
- [ ] Vincent: reach out to potential candidate who has expressed interest in joining operations team
- [ ] Vincent: (possibly as part of onboarding operations team member?) document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.

