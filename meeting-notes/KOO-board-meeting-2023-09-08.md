# KOO Board Meeting 2023-09-08


## Attendees

- Ola (taking notes)
- Daniel
- Neal

Absent with excuse:
- Lukas
- Vincent


## Notes

- No followups from previous meetings

### KEP proposals for v6 keys

(Daniel summarizes the proposal from Vincent, and the proposal by Daniel sent to email lists)
- Proposal from Vincent involves separate namespaces for v4 and v6, cloning the API.
- Proposal from Daniel is based on the idea of having one single key block with potentially multiple keys for different versions, or with different algorithms.

Neal: exists one more proposal that is not KEP: wait for HKP to be standardized.
Daniel: means deprecating current KOO API. We should ask V for reasons for creating original custom API for KOO.
Neal: Biggest difference is that KOO only returns one certificate.
Daniel: KOO implementation of HKP only returns one certificate as well.
Daniel: part of his proposal, another part is to fetch keys by hash, instead of by email. this is done to offer some kind of privacy in case of negative response. by having our own API, these kinds of tweaks are easier for us to add.
Neal: but if new version of HKP is coming...
Neal: current API: https://keys.openpgp.org/about/api
Neal: no substring search on uid field
Neal: If there is work coming on HKP for adding new things and standardize, we should join forces.
Neal: feels that proposal by Daniel adds complexity
Neal: if a user uploads a keyring and another user downloads that keyring, you have to trust the server to not mess with it
Neal: in terms of regular certificates, self-sigs protect against changes, but for the keyring model, not possible to verify
Daniel: what if using the latest key?
Neal: shouldn't be the decision of KOO
Daniel: wants policy in proposal in order to clarify decisions
Neal: different people have different perspectives, for example Neal and Vincent look on policy very different. there will never be total agreement, which is fine. for this reason KOO shouldn't encode the policy decision.
Daniel: if we don't say anything about it, if one client always uses the newest key, and another uses the first key, etc, this might cause compatibility issues.
Neal: no third-party should make auth decisions for me.
Daniel: just because the server puts it in order, doesn't mean the client should trust it.
Ola: agree, KOO shouldn't specify policy.
Daniel: it would be good to have it said which key should be used
Ola: this could be a different proposal/specification that has nothing to do with KOO, that clients can use
Daniel: clients uploading might want to have influence on what keys should be used. for example, use my PQC key but if you don't understand it, use this one.
Neal: if PQC isn't supported, the key will be ignored
Neal: you can add a subkey which specify orders or preferences
Ola: doesn't that just move the problem, but still require the client to decide

(discussion gets into the weeds...)

Daniel: interesting discussion about the different proposals: how do we store the keys, and should there be multiple keys. do we allow multiple keys per version.
Neal: thinks it's a mistake to only return one key. lost this battle long time ago. wants to revisit this decision.
Daniel: When PQC keys arrive, would we have to duplicate/clone the API again, if we go with the simple namespace solution? We might need to revisit the discussion when Vincent returns. 
Neal: KOO shouldn't make auth decisions, unless we make it a CA - which I'm happy to do.
Daniel: if you don't trust KOO to set an ordering, why would you trust it to be a CA?
Neal: I don't. I look for evidence of truthiness, and I use different pieces of evidence and combine with other pieces to make a decision. other issue is that the ordering is not signed by anyone. ideally, user should sign ordering. in this scenario, KOO can only withhold information, but not change the ordering. problem with the proposal from Daniel is that there is no auth of the ordering, which means different clients could see different results from KOO
Ola: KOO should return the certificates in a random order to force the clients to make the decision.  The clients should decide based on what is in the certificates or something external; but the client should not rely on KOO for authentication information.  I do think that there is a place for a preference as Neal was talking about, but it should not be based on what KOO returns.
Daniel: It would be good to have the community to come up with some policy that is good for most people.  I think ordering by creation time will usually be sufficient.
Ola: The newest key might be from an attacker.  The problem is that certificates are standalone.  We need a way to tie them together.
Daniel: Our Key transparency implementation has a list of keys.  It is signed by the first key.  That's not perfect, but the owner checks it regularly.
Ola: key transparency makes attacks visible, but doesn't stop them. still think a there's a place for tying together a key bag in some way.
Daniel: if we don't trust key servers, a key server can always generate a new key.
Ola: would be useful for clients to have a way of deciding which key to use. maybe new charter for IETF working group can include this?
Daniel: the owner of a key should make the decision
Ola: agreed, but KOO shouldn't be intermediary for that
Daniel: who should be
Ola: no-one. the client should be able to figure it out without intermediary
Daniel: for the new working group, there are a lot of topics already. there might not be enough bandwidth. there hasn't been any feedback on several proposals. WG not chartered yet, officially, so the situation will hopefully change.
Neal: it's tricky. there are many layers to this issue. specs, implementations, etc
Daniel: implemented both proposals in our client


### Election

Neal: we took office in December (2022). voting started Oct 20th. six weeks from now. oct 7 was self-nomination. We have one month. 
Daniel: Should Lukas organize this?
Neal: Yes, the secretary does it.
Daniel: Do we want to change anything to the process?
Neal: There was interest in secret ballot. but that is complicated. can we achieve that in a month?
Daniel: how?
Neal: tooling. designation of trust.
Daniel: since secretary is in charge, there might be an issue with sending votes to secretary if secretary also self-nominates. should we ask external person to do it again? 
Ola: send votes to more than one person to avoid single point of trust?
Daniel: still a problem with sending to secretary
Ola: according to statutes, can secretary delegate to three people to receive votes?
Daniel: we can propose changes to constitution for this.
Neal: ok with status quo. if we invest any energy, we should recruit additional people to voting body.
Daniel: there was an article about governance. nothing about voting body. might be a good idea to do that now. 
Ola: can we wait, or should we start mail about these issues?

ACTION: write email to board to discuss voting process, and how to increase voting body. (Assigned to Neal)

Email used for self-nominating the first time:

To: koo-voting@enigmail.net
Subject: [Koo-voting] Invitation to Self-Nomination for Initial KOO Board
From: Patrick Brunschwig <patrick@enigmail.net>
Delivery-date: Fri, 07 Oct 2022 15:59:05 +0200
Archived-At: <https://lists.hostpoint.ch/archives/list/koo-voting@enigmail.net/message/D5AYJ3QJVCMWXBJM5E6Q2FKSPXA3RVC5/>
Message-ID: <7a0e1cc6-72a3-6f3c-e7c8-29ab3059cbee@enigmail.net>

 - Today (October 7, 2022) we open the self-nomination process.  Any of
   the voting members can present themselves as a candidate for the
   KOO Board.

 - On October 21,2022 we will begin the voting process. Each member of
   the voting body can indicate which candidates they approve of as a
   potential Board member.

 - On November 4, 2022, the election organizer (that's me, Patrick) will
   announce the end of voting.

 - No later than November 11, 2022, I will confirm the five most-
   approved candidates to constitute the new Board.

 - The new Board assumes its duties on December 1, 2022.

Replace 2022 by 2023.


### Progress

- Daniel: Should we make have more regular meetings?
- Daniel: possibility of person joining ops team. would be possible if progress on one of the KEPs happen. even of HKP makes progress, there might be things to do
- Neal: there are other topics we've discussed.
- Daniel: decisions would be good. no quorum for that in this meeting.


## Next meeting

- Daniel traveling for two weeks
- Unsure if Vincent and Lukas can, but we propose next week, September 15.
- Meeting also planned for September 29.


## Scratch section

https://keys.openpgp.org/about/api
