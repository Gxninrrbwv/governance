Present: Neal, Daniel, Ola, Vincent
Absent:
Absent with excuses: Lukas

# Agenda

* Discuss Privacy policy changes
* V6 support in KOO


# Discuss Privacy policy changes

Vincent prepared a draft at https://gitlab.com/keys.openpgp.org/hagrid/-/merge_requests/205

Daniel: clarify requests logging?
-> yup

Vincent will add some changes to the privacy policy based on the discussion in the meeting.  He will upload the change tonight (Friday), and if there are no objections, then will merge it not before Wednesday evening.


# V6 support in KOO

We should add v6 support as soon as possible.  The main piece of work is to define the semantics of the routes.

Some details from the recent email summit were summarized by Daniel in an email to the board.  First step should be alignment on the basic design.  This will be done via email and discussed in the next meeting. The second step is to write up the proposal as a KEP and get feedback.
